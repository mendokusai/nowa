const { NowaRequest } = require('../nowaRequest');

class DbInitializer {
	constructor() {
		this.request = new NowaRequest();
	}

	async init() {
		try {
			await this.request.post('/db/init');
		} catch (e) {
			console.error(`\n\n${JSON.stringify(e)}\n\n`);
			throw e;
		}
	}
}

module.exports = { DbInitializer };
