const assert = require('assert').strict;

module.exports = (e, expectedStatusCode, expectedCode) => {
	assert.equal(e.statusCode, expectedStatusCode);
	assert.ok(e.error, 'e.error must be defined');
	assert.ok(e.error.err, 'e.error.err must be defined');

	const { id, message, details, code, httpStatusCode, stack } = e.error.err;
	assert.equal(typeof id, 'string');
	assert.equal(typeof message, 'string');
	assert.equal(typeof details, 'string');
	assert.equal(typeof code, 'string');
	assert.equal(typeof httpStatusCode, 'number');
	assert.equal(typeof stack, 'string');

	assert.equal(httpStatusCode, expectedStatusCode);
	assert.equal(code, expectedCode);
};
