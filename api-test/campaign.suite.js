const assert = require('assert').strict;

const { Suite } = require('doogh');
const { NowaRequest } = require('./nowaRequest');

const { checkError } = require('./utils');

const ts = new Suite();
const request = new NowaRequest();

ts.test('Must throw token is missing error', async () => {
	try {
		await request.post('/campaigns', {
			campaign: { title: 'kooft' },
		});
	} catch (e) {
		checkError(e, 401, 'TOKEN_IS_MISSING');
	}
});

ts.test('Send request to dm\'s own campaign', async (wm) => {
	const dm = await wm.createUser();
	const campaign = await wm.createCampaign({ dm });
	try {
		await wm.requestToJoinToCampaign(dm, campaign.code);
	} catch (e) {
		checkError(e, 400, 'ALREADY_JOINED_TO_CAMPAIGN');
	}
});

ts.test('Request is rejected by DM', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	await joinCampaignRequest.reject();
});

ts.test('Request is rejected by Dungeoneer.. must throw an error', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	try {
		await joinCampaignRequest.reject(dungeoneer);
	} catch (e) {
		checkError(e, 403, 'UNAUTHORIZED');
	}
});

ts.test('Request is canceled by Dungeoneer', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	await joinCampaignRequest.cancel();
});


ts.test('Request is canceled by DM.. must throw an error', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	try {
		await joinCampaignRequest.cancel();
	} catch (e) {
		checkError(e, 403, 'UNAUTHORIZED');
	}
});

ts.test('Accept the join request', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	await joinCampaignRequest.accept();
});

ts.test('Accept the join request by Dungeoneer.. must throw an error', async (wm) => {
	const { campaign, dungeoneer } = await wm.prepareForRequest();
	const joinCampaignRequest = await wm.requestToJoinToCampaign(dungeoneer, campaign.code);
	try {
		await joinCampaignRequest.accept(dungeoneer);
	} catch (e) {
		checkError(e, 403, 'UNAUTHORIZED');
	}
});

module.exports = ts;
