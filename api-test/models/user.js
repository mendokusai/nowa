const assert = require('assert').strict;

const { NowaRequest } = require('../nowaRequest');

const USER_ROLES = Object.freeze({
	ADMIN: 'ADMIN',
	DUNGEONEER: 'DUNGEONEER',
});

class User {
	constructor({ firstName = 'Zuriel', lastName = 'Kraushaar', email = 'zuriel.kraushaar@siahchal.com',
		password = 'mystupidpassword', role = USER_ROLES.DUNGEONEER }) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.role = role;

		this.request = new NowaRequest();
	}

	async login(password) {
		const _password = password || this.password;
		const result = await this.request.post('/users/login', {
			loginData: {
				email: this.email,
				password: _password,
			},
		});
		assert.equal(typeof result.token, 'string');
		return result;
	}

	async register() {
		const result = await this.request.post('/users/register', {
			userData: {
				firstName: this.firstName,
				lastName: this.lastName,
				email: this.email,
				password: this.password,
			},
		});
		assert.equal(typeof result.token, 'string');

		this.request.setAuthToken(result.token);

		return result;
	}

	async createCampaign(title = 'My New Campaign', isPublic = false,
		description = 'A bunch of nerdy-ass goashaad developers trying to have fun') {
		const result = await this.request.post('/campaigns', {
			campaignData: {
				title,
				description,
				isPublic,
			},
		});

		assert.equal(typeof result.campaign, 'object');
		const { campaign } = result;
		const { id, dmId, adminId, code, createdAt } = campaign;

		assert.equal(typeof id, 'string');
		assert.equal(typeof dmId, 'string');
		assert.equal(typeof adminId, 'string');
		assert.equal(dmId, adminId);
		assert.equal(typeof code, 'string');
		assert.equal(typeof createdAt, 'string');
		assert.equal(title, campaign.title);
		assert.equal(description, campaign.description);
		assert.equal(isPublic, campaign.isPublic);

		return result;
	}

	async requestToJoinToCampaign(campaignCode) {
		const result = await this.request.post(`/campaigns/${campaignCode}/join`);
		assert.equal(typeof result.joinCampaignRequest, 'object');
		const { joinCampaignRequest } = result;
		const { id, userId, campaignId, status, createdAt } = joinCampaignRequest;

		assert.equal(typeof id, 'string');
		assert.equal(typeof userId, 'string');
		assert.equal(typeof campaignId, 'string');
		assert.equal(typeof status, 'string');
		assert.equal(status, 'INITIATED');
		assert.equal(typeof createdAt, 'string');

		return result;
	}
}

module.exports = { User };
