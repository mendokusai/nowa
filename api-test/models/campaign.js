class Campaign {
	constructor({ id, code, admin, dm, title, description, isPublic }) {
		this.id = id;
		this.code = code;
		this.title = title;
		this.description = description;
		this.admin = admin;
		this.dm = dm;
		this.isPublic = isPublic;
	}
}

module.exports = { Campaign };
