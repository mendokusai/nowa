const assert = require('assert').strict;

class JoinCampaignRequest {
	constructor({ id, user, campaign, status }) {
		this.id = id;
		this.user = user;
		this.campaign = campaign;
		this.status = status;
	}

	async accept(dm) {
		const _dm = dm || this.campaign.dm;
		const result = await _dm.request.put(`/joinCampaignRequests/${this.id}/accept`);
		assert.equal(typeof result.joinCampaignRequest, 'object');

		const { joinCampaignRequest } = result;
		assert.equal(joinCampaignRequest.id, this.id);
		assert.equal(joinCampaignRequest.status, 'ACCEPTED');

		return result;
	}

	async reject(dm) {
		const _dm = dm || this.campaign.dm;
		const result = await _dm.request.put(`/joinCampaignRequests/${this.id}/reject`);
		assert.equal(typeof result.joinCampaignRequest, 'object');

		const { joinCampaignRequest } = result;
		assert.equal(joinCampaignRequest.id, this.id);
		assert.equal(joinCampaignRequest.status, 'REJECTED');

		return result;
	}

	async cancel(dungeoneer) {
		const _dungeoneer = dungeoneer || this.user;
		const result = await _dungeoneer.request.put(`/joinCampaignRequests/${this.id}/cancel`);
		assert.equal(typeof result.joinCampaignRequest, 'object');

		const { joinCampaignRequest } = result;
		assert.equal(joinCampaignRequest.id, this.id);
		assert.equal(joinCampaignRequest.status, 'CANCELED');

		return result;
	}
}

module.exports = { JoinCampaignRequest };
