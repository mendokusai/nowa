const Promise = require('bluebird');

class WorldModel {
	async init() {
	}

	async waitOnConnect() {
		await Promise.delay(100); // some random delay ensuring that the services are up and running
	}

	async environmentCheck() {
	}
	
	async tearDown() {
	}
}


module.exports = WorldModel;
