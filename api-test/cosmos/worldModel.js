const assert = require('assert').strict;

const Promise = require('bluebird');
const doogh = require('doogh');

const { User } = require('../models/user');
const { Campaign } = require('../models/campaign');
const { JoinCampaignRequest } = require('../models/joinCampaignRequest');

const { DbInitializer } = require('../utils/dbInitializer');

class WorldModel extends doogh.WorldModel {
	constructor() {
		super();
		this.dbInitializer = new DbInitializer();
	}

	async init() {
		await this.dbInitializer.init();
		this.campaigns = [];
		this.users = [];
	}

	async waitOnConnect() {
		await Promise.delay(3000);
	}

	async createUser({ firstName, lastName, email, password } = {}) {
		const _user = new User({ firstName, lastName, email, password });
		await _user.register();
		this.users.push(_user);
		return _user;
	}

	// TODO GET campaigns must be called in this function
	async createCampaign({ dm, title, description, isPublic }) {
		const { campaign } = await dm.createCampaign(title, description, isPublic);

		const _campaign = new Campaign({
			...campaign,
			admin: dm,
			dm,
		});
		this.campaigns.push(_campaign);
		return _campaign;
	}

	// TODO GET requestToJoinToCampaign must be called in this function
	async requestToJoinToCampaign(user, campaignCode) {
		const { joinCampaignRequest } = await user.requestToJoinToCampaign(campaignCode);

		const _campaign = this.campaigns.find((c) => c.code === campaignCode);
		assert.equal(_campaign.id, joinCampaignRequest.campaignId);

		const _joinCampaignRequest = new JoinCampaignRequest({
			...joinCampaignRequest,
			user,
			campaign: _campaign,
		});
		return _joinCampaignRequest;
	}

	// TODO get dunngeoneer's count
	async prepareForRequest() {
		const dm = await this.createUser();

		const campaign = await this.createCampaign({ dm });

		const dungeoneer = await this.createUser({
			firstName: 'Dante',
			lastName: 'Namsilat',
			email: 'dante.namsilat@siahchal.com',
			password: '12345',
		});

		return {
			dm,
			campaign,
			dungeoneer,
		};
	}
}

module.exports = { WorldModel };
