const events = require('events');

events.defaultMaxListeners = 1000;
const path = require('path');
const { execSync } = require('child_process');

const doogh = require('doogh');

const { Services: { JsService } } = doogh;

const { WorldModel } = require('./cosmos/worldModel');

const CampaignSuite = require('./campaign.suite');

const MONGO_ADDR = 'mongodb://127.0.0.1:27017/nowa';

// const jsServices = [
// 	'nowa',
// ];

const servicesEnv = {
	nowa: {
		MONGO_URL: MONGO_ADDR,
		PORT: 9002,
	},
};


// let nodePath;
// try {
// 	nodePath = execSync('which node').toString().replace(/\r?\n|\r/g, '');
// } catch (e) {
// 	console.error(`Could not find node executable.\n${e.message}`);
// 	process.exit(1);
// }

let npmPath;
try {
	npmPath = execSync('which npm').toString().replace(/\r?\n|\r/g, '');
} catch (e) {
	console.error(`Could not find npm path.\n${e.message}`);
	process.exit(1);
}

const nowaPath = path.join(__dirname, '..', 'core').toString();
doogh.submitService(new JsService('nowa', nowaPath, 'start', [], {
	NODE_ENV: 'develop',
	additionalEnv: servicesEnv.nowa,
}, npmPath));



// jsServices.forEach((name) => {
// 	const servicePath = path.join(__dirname, '../').toString();
// 	const additionalEnv = servicesEnv[name] || {};
// 	doogh.submitService(new JsService(name, servicePath, 'start', [], {
// 		NODE_ENV: 'develop',
// 		...additionalEnv,
// 	}, npmPath));
// });

doogh.addSuite(CampaignSuite);

doogh.register(new WorldModel());

doogh.run();

process.on('exit', () => {
	doogh.killThemAll();
});
