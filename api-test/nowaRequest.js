const doogh = require('doogh');

class NowaRequest extends doogh.Request {
	constructor() {
		super('http://localhost:9002'); // TODO get it from env variables
		this.defaultHeaders = { 'content-type': 'application/json' };
		this.authorization = null;
	}

	setAuthToken(token) {
		this.defaultHeaders.Authorization = token;
		this.authorization = token;
	}
}

module.exports = { NowaRequest };
