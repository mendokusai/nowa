const { logger } = require('./lib/logger');

const { Service } = require('./src/service');

async function run() {
	const service = new Service();

	try {
		await service.init();
		await service.start();
	} catch (e) {
		logger.error(`Error during initialization:  ${e.stack || e.toString()}`);
	}

	return service;
}

module.exports = run();
