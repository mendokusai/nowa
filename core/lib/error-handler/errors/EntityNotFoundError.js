const GeneralError = require('./GeneralError');
const ErrorCodes = require('./codes');

class EntityNotFoundError extends GeneralError {
	constructor({ id, code = ErrorCodes.ENTITY_NOT_FOUND, httpStatusCode = 404, details, originalError, entityName, data }) {
		super({
			id,
			code,
			httpStatusCode,
			details,
			originalError,
			message: `${entityName} not found :: data: \n ${JSON.stringify(data)}`,
		});
	}
}

module.exports = EntityNotFoundError;
