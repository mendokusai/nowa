const GeneralError = require('./GeneralError');
const EntityAlreadyExistedError = require('./EntityAlreadyExistedError');
const EntityNotFoundError = require('./EntityNotFoundError');
const ErrorCodes = require('./codes');

module.exports = {
	GeneralError,
	ErrorCodes,
	EntityAlreadyExistedError,
	EntityNotFoundError,
};
