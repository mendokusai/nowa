const uuid = require('uuid/v4');

class GeneralError extends Error {
	constructor({
		id = uuid(),
		message,
		code,
		httpStatusCode = 500,
		details,
		originalError,
	}) {
		super(message);

		this.id = id;
		this.code = code;
		this.httpStatusCode = httpStatusCode;
		this.details = details;
		this.originalError = originalError;

		this.name = this.constructor.name;

		Error.captureStackTrace(this, this.constructor);

		if (this.originalError && this.originalError.stack) {
			this.stack += ` :: ${this.originalError.stack}`;
		}
	}

	toString() {
		let res = super.toString();
		if (this.originalError) {
			const originalErrorStr = this.originalError.toString();
			if (originalErrorStr !== '[object Object]') {
				res += ` :: ${originalErrorStr}`;
			}
		}

		if (this.code) {
			res += ` :: code: ${this.code}`;
		}

		res += ` :: errorId: ${this.id}`;

		if (this.stack) {
			res += ` :: stack: ${this.stack}`;
		}

		return res;
	}

	toJSON() {
		return {
			id: this.id,
			message: this.message,
			details: this.details || '',
			code: this.code,
			httpStatusCode: this.httpStatusCode,
			stack: process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production' ? undefined : this.stack,
		};
	}
}

module.exports = GeneralError;
