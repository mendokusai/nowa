const GeneralError = require('./GeneralError');
const ErrorCodes = require('./codes');

class EntityAlreadyExistedError extends GeneralError {
	constructor({ id, code = ErrorCodes.ENTITY_ALREADY_EXISTED, httpStatusCode = 400, details, originalError, entityName, data }) {
		super({
			id,
			code,
			httpStatusCode,
			details,
			originalError,
			message: `${entityName} is already existed :: data: \n ${JSON.stringify(data)}`,
		});
	}
}

module.exports = EntityAlreadyExistedError;
