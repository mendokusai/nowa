const assert = require('assert');

const { GeneralError, ErrorCodes } = require('./errors');

function determineErrorCode(err) {
	if (err.status === 400 || err.statusCode === 400) {
		return ErrorCodes.BAD_REQUEST;
	}
	if (err.status === 404 || err.statusCode === 404) {
		return ErrorCodes.NOT_FOUND;
	}
	return ErrorCodes.UNKNOWN_ERR;
}

function handleError(logger) {
	assert(logger, 'expected logger to be passed.');

	return (err, req, res, next) => {
		let e = null;
		if (!err.toJSON) {
			e = new GeneralError({
				message: err.message,
				code: err.code || determineErrorCode(err),
				httpStatusCode: err.httpStatusCode || err.status || err.statusCode || 500,
				originalError: err,
			});
		} else {
			e = err;
		}

		logger.error(`Error during request: ${req.method} ${req.url} => ${e.toString()}`);

		res.status(e.httpStatusCode || 500).json({ err: e.toJSON() });
	};
}

module.exports = { handleError, GeneralError };
