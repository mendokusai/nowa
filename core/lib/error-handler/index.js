const { handleError } = require('./handler');
const {
	EntityNotFoundError,
	EntityAlreadyExistedError,
	GeneralError,
} = require('./errors');

module.exports = {
	errorHandler: handleError,
	GeneralError,
	EntityAlreadyExistedError,
	EntityNotFoundError,
};
