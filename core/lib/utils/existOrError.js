const assert = require('assert');

function existOrError(arg, argName) {
	assert(arg, `expected ${argName} to be passed.`);
}

module.exports = existOrError;