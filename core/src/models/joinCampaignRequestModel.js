const mongoose = require('mongoose');

const { Schema } = mongoose;

const JOIN_CAMPAIGN_REQUEST_STATUSES = Object.freeze({
	INITIATED: 'INITIATED',
	ACCEPTED: 'ACCEPTED',
	REJECTED: 'REJECTED',
	CANCELED: 'CANCELED',
});

const JoinCampaignRequestSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
		index: true,
	},
	campaignId: {
		type: Schema.Types.ObjectId,
		ref: 'Campaign',
		required: true,
		index: true,
	},
	status: {
		type: String,
		enum: Object.values(JOIN_CAMPAIGN_REQUEST_STATUSES),
		default: JOIN_CAMPAIGN_REQUEST_STATUSES.INITIATED,
		required: true,
		index: true,
	},
}, { timestamps: true });

const JoinCampaignRequest = mongoose.model('JoinCampaignRequest', JoinCampaignRequestSchema);

// TODO we need another index for ACCEPTED
JoinCampaignRequest.collection.createIndex({
	userId: 1,
	campaignId: 1,
	status: 1,
}, {
	unique: true,
	partialFilterExpression: {
		status: JOIN_CAMPAIGN_REQUEST_STATUSES.INITIATED,
	},
});

module.exports = {
	JoinCampaignRequest,
	JOIN_CAMPAIGN_REQUEST_STATUSES,
};
