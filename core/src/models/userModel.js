const mongoose = require('mongoose');

const { Schema } = mongoose;

const USER_ROLES = Object.freeze({
	ADMIN: 'ADMIN',
	DUNGEONEER: 'DUNGEONEER',
});

const UserSchema = new Schema({
	email: {
		type: String,
		unique: true,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	lastLogin: Date,
	role: {
		type: String,
		enum: Object.values(USER_ROLES),
		default: USER_ROLES.ADMIN,
		required: true,
	},
	// TODO add gender, birthday, etc
}, { timestamps: true });

const User = mongoose.model('User', UserSchema);

module.exports = {
	User,
	USER_ROLES,
};
