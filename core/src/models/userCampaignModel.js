
const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserCampaignSchema = new Schema({
	campaignId: {
		type: Schema.Types.ObjectId,
		ref: 'Campaign',
		index: true,
		required: true,
	},
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		index: true,
		required: true,
	},
}, { timestamps: true });

const UserCampaign = mongoose.model('UserCampaign', UserCampaignSchema);

UserCampaign.collection.createIndex({
	campaignId: 1,
	userId: 1,
}, { unique: true });

module.exports = { UserCampaign };
