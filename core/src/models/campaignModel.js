
const mongoose = require('mongoose');

const { Schema } = mongoose;

const CampaignSchema = new Schema({
	code: {
		type: String,
		unique: true,
		required: true,
	},
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: false,
	},
	avatarUrl: String,
	adminId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		index: true,
		required: true,
	},
	dmId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		index: true,
		required: true,
	},
	isPublic: {
		type: Boolean,
		default: false,
		index: true,
		required: true,
	},
}, { timestamps: true });

const Campaign = mongoose.model('Campaign', CampaignSchema);

module.exports = { Campaign };
