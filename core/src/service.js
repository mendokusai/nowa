const { Assembly } = require('./assembly');

class Service {
	constructor() {
		this.assembly = new Assembly();
	}

	async init() {
		await this.assembly.init();
	}

	async start() {
		try {
			const { router, httpServer, config } = this.assembly;
			const serverPort = config.port;

			await router.registerRoutes();
			this.httpServer = httpServer.listen(serverPort);
			console.info(`Service is running on http://localhost:${serverPort}`);
		} catch (e) {
			console.error(e);
		}
	}
}

module.exports = {
	Service,
};
