const { existOrError } = require('../../lib/utils');

class CampaignMapper {
	static fromModelToDto({ _id, dmId, adminId, code, title, description, isPublic, createdAt }) {
		existOrError(_id, '_id');
		return {
			id: _id.toString(),
			dmId: dmId.toString(),
			adminId: adminId.toString(),
			createdAt: new Date(createdAt).toISOString(),
			code,
			title,
			description,
			isPublic,
		};
	}
}

module.exports = { CampaignMapper };
