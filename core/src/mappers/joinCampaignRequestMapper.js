const { existOrError } = require('../../lib/utils');

class JoinCampaignRequestMapper {
	static fromModelToDto({ _id, userId, campaignId, status, createdAt }) {
		existOrError(_id, '_id');
		return {
			id: _id.toString(),
			userId: userId.toString(),
			campaignId: campaignId.toString(),
			createdAt: new Date(createdAt).toISOString(),
			status,
		};
	}
}

module.exports = { JoinCampaignRequestMapper };