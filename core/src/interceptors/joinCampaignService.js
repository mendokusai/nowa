const { existOrError } = require('../../lib/utils');
const { EntityAlreadyExistedError, GeneralError, EntityNotFoundError } = require('../../lib/error-handler');

const { Campaign } = require('../models/campaignModel');
const { JoinCampaignRequest, JOIN_CAMPAIGN_REQUEST_STATUSES } = require('../models/joinCampaignRequestModel');
const { UserCampaign } = require('../models/userCampaignModel');

const ROLES = {
	DM: 'DM',
	DUNGEONEER: 'DUNGEONEER',
};

class JoinCampaignService {
	constructor({ errors, logger }) {
		existOrError(errors, 'errors');

		this.errors = errors;
		this.logger = logger;
	}

	async requestToJoin(userId, campaignCode) {
		existOrError(userId, 'userId');
		existOrError(campaignCode, 'campaignCode');

		const campaign = await Campaign.findOne({ code: campaignCode }).exec();

		// TODO campaign not found
		if (!campaign) {
			throw new EntityNotFoundError({
				entityName: 'Campaign',
				data: {
					code: campaignCode,
				},
			});
		}

		if (campaign.adminId.toString() === userId || campaign.dmId.toString() === userId) {
			throw new GeneralError(this.errors.ALREADY_JOINED_TO_CAMPAIGN);
		}

		const userCampaign = await UserCampaign.findOne({
			campaignId: campaign.id,
			userId,
		}).lean().exec();

		if (userCampaign) {
			throw new GeneralError(this.errors.ALREADY_JOINED_TO_CAMPAIGN);
		}

		const hasPreviousOpenOrAcceptedRequest = await JoinCampaignRequest.exists({
			status: {
				$in: [
					JOIN_CAMPAIGN_REQUEST_STATUSES.INITIATED,
					JOIN_CAMPAIGN_REQUEST_STATUSES.ACCEPTED,
				],
			},
			campaignId: campaign.id,
			userId,
		});
		if (hasPreviousOpenOrAcceptedRequest) {
			throw new EntityAlreadyExistedError({
				entityName: 'JoinCampaignRequest',
				data: {
					campaignId: campaign.id,
					userId,
					additionalNote: 'hasPreviousOpenOrAcceptedRequest',
				},
			});
		}

		const joinCampaignRequest = await JoinCampaignRequest.create({
			status: JOIN_CAMPAIGN_REQUEST_STATUSES.INITIATED,
			campaignId: campaign.id,
			userId,
		});

		return { joinCampaignRequest };
	}

	async acceptRequest(userId, joinCampaignRequestId) {
		existOrError(userId, 'userId');
		existOrError(joinCampaignRequestId, 'joinCampaignRequestId');

		const joinCampaignRequest = await this._getJoinCampaignRequestByIdOrError(joinCampaignRequestId);

		await this._checkIfUserIsAdminOrDmOfCampaign(userId, joinCampaignRequest);

		const userCampaign = await UserCampaign.findOne({
			campaignId: joinCampaignRequest.campaignId,
			userId,
		}).lean().exec();

		if (!userCampaign) {
			await UserCampaign.create({
				campaignId: joinCampaignRequest.campaignId,
				userId,
			});
		}

		joinCampaignRequest.status = JOIN_CAMPAIGN_REQUEST_STATUSES.ACCEPTED;
		const updatedJoinCampaignRequest = await joinCampaignRequest.save();

		return { joinCampaignRequest: updatedJoinCampaignRequest };
	}

	async rejectRequest(userId, joinCampaignRequestId) {
		existOrError(userId, 'userId');
		existOrError(joinCampaignRequestId, 'joinCampaignRequestId');

		const joinCampaignRequest = await this._getJoinCampaignRequestByIdOrError(joinCampaignRequestId);

		await this._checkIfUserIsAdminOrDmOfCampaign(userId, joinCampaignRequest);

		joinCampaignRequest.status = JOIN_CAMPAIGN_REQUEST_STATUSES.REJECTED;
		const updatedJoinCampaignRequest = await joinCampaignRequest.save();

		return { joinCampaignRequest: updatedJoinCampaignRequest };
	}

	async cancelRequest(userId, joinCampaignRequestId) {
		existOrError(userId, 'userId');
		existOrError(joinCampaignRequestId, 'joinCampaignRequestId');

		const joinCampaignRequest = await this._getJoinCampaignRequestByIdOrError(joinCampaignRequestId);

		if (joinCampaignRequest.userId.toString() !== userId) {
			throw new GeneralError(this.errors.UNAUTHORIZED);
		}

		joinCampaignRequest.status = JOIN_CAMPAIGN_REQUEST_STATUSES.CANCELED;
		const updatedJoinCampaignRequest = await joinCampaignRequest.save();

		return { joinCampaignRequest: updatedJoinCampaignRequest };
	}

	async getRequests(userId, role, status) {
		existOrError(userId, 'userId');
		existOrError(role, 'role');
		existOrError(status, 'status');

		if (role === ROLES.DM) {
			return this._getDmRequests(userId, status);
		}
		if (role === ROLES.DUNGEONEER) {
			return this._getDungeoneerRequests(userId, status);
		}
		throw new GeneralError(this.errors.BAD_REQUEST);
	}

	async _getDmRequests(dmId, status) {
		existOrError(dmId, 'dmId');
		existOrError(status, 'status');

		const campaigns = await Campaign.find({
			$or: [
				{ adminId: dmId },
				{ dmId },
			],
		}).lean().exec();
		const campaignIds = campaigns.map((c) => c._id);
		const joinCampaignRequests = await JoinCampaignRequest.find({
			status,
			campaignId: { $in: campaignIds },
		}).lean().exec();
		// TODO joinCampaignRequest should have a shadow of user data and campaign data
		return { joinCampaignRequests };
	}

	async _getDungeoneerRequests(dungeoneerId, status) {
		existOrError(dungeoneerId, 'dungeoneerId');
		existOrError(status, 'status');

		const joinCampaignRequests = await JoinCampaignRequest.find({
			userId: dungeoneerId,
			status: JOIN_CAMPAIGN_REQUEST_STATUSES.INITIATED,
		}).lean().exec();

		return { joinCampaignRequests };
	}

	async _checkIfUserIsAdminOrDmOfCampaign(userId, joinCampaignRequest) {
		existOrError(userId, 'userId');
		existOrError(joinCampaignRequest, 'joinCampaignRequest');

		const campaign = await Campaign.findById(joinCampaignRequest.campaignId).lean().exec();
		if (!campaign) {
			throw new EntityNotFoundError({
				entityName: 'Campaign',
				data: {
					id: joinCampaignRequest.campaignId,
				},
			});
		}

		if (campaign.adminId.toString() !== userId && campaign.dmId.toString() !== userId) {
			throw new GeneralError(this.errors.UNAUTHORIZED);
		}
	}

	async _getJoinCampaignRequestByIdOrError(joinCampaignRequestId) {
		existOrError(joinCampaignRequestId, 'joinCampaignRequestId');

		const joinCampaignRequest = await JoinCampaignRequest.findById(joinCampaignRequestId).exec();
		if (!joinCampaignRequest) {
			throw new EntityNotFoundError({
				entityName: 'JoinCampaignRequest',
				data: {
					id: joinCampaignRequestId,
				},
			});
		}
		return joinCampaignRequest;
	}
}

module.exports = { JoinCampaignService };
