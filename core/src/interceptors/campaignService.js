
const shortid = require('shortid');

const { existOrError } = require('../../lib/utils');
const { Campaign } = require('../models/campaignModel');
const { UserCampaign } = require('../models/userCampaignModel');

class CampaignService {
	async createCampaign({ title, description, avatarUrl, userId, isPublic }) {
		existOrError(userId, 'userId');
		existOrError(title, 'title');
		existOrError(description, 'description');

		const code = await this._generateCode();
		const campaign = await Campaign.create({
			code,
			title,
			description,
			avatarUrl,
			dmId: userId,
			adminId: userId,
			isPublic,
		});

		await UserCampaign.create({
			campaignId: campaign.id,
			userId,
		});

		return { campaign };
	}

	// TODO add pagination
	async getCampaignsOfUser(userId) {
		const userCampaigns = await UserCampaign.find({ userId }).lean().exec();
		const campaignsIds = userCampaigns.map((uc) => uc.campaignId);
		const campaigns = await Campaign.find({ _id: { $in: campaignsIds } }).lean().exec();
		return { campaigns };
	}

	// A service must generate codes and we request one code from the service.
	async _generateCode() {
		let code = null;
		let isNotUnique = true;
		while (isNotUnique) {
			code = shortid.generate();
			/* eslint-disable no-await-in-loop */
			isNotUnique = await Campaign.exists({ code });
			/* eslint-enable no-await-in-loop */
		}
		return code;
	}
}

module.exports = { CampaignService };
