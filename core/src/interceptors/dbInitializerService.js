const Promise = require('bluebird');

class DbInitializerService {
	constructor({ connections: { mongo } }) {
		this.mongo = mongo;
	}

	async init() {
		return Promise.all(this.mongo.db.collections().map((collection) => collection.deleteMany({})));
	}
}

module.exports = { DbInitializerService };
