const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Promise = require('bluebird');

const { existOrError } = require('../../lib/utils');
const { GeneralError } = require('../../lib/error-handler');

const { User, USER_ROLES } = require('../models/userModel');

const jwtSignAsync = Promise.promisify(jwt.sign);

class UserService {
	constructor({ errors, config }) {
		existOrError(errors, 'errors');
		existOrError(errors, 'config');

		this.errors = errors;
		this.config = config;
	}

	// TODO expiration of token
	async login(email, password) {
		existOrError(email, 'email');
		existOrError(password, 'password');

		const user = User.findOne({ email }).lean().exec();
		if (!user) {
			throw new GeneralError(this.errors.EMAIL_OR_PASSWORD_IS_NOT_CORRECT);
		}

		const passwordMatched = await bcrypt.compare(password, user.password);
		if (!passwordMatched) {
			throw new GeneralError(this.errors.EMAIL_OR_PASSWORD_IS_NOT_CORRECT);
		}

		const token = await this._createToken(user);
		return { token };
	}

	// TODO verify email
	// TODO the token must be temporary
	async register({ firstName, lastName, email, password }) {
		existOrError(firstName, 'firstName');
		existOrError(lastName, 'lastName');
		existOrError(email, 'email');
		existOrError(password, 'password');

		const hashedPassword = await bcrypt.hash(password, this.config.bcrypt.saltRounds);
		const user = await User.create({
			firstName,
			lastName,
			password: hashedPassword,
			email,
			role: USER_ROLES.DUNGEONEER,
		});
		const token = await this._createToken(user);
		user.lastLogin = new Date();
		await user.save();
		return { token };
	}

	async _createToken({ id, role, email, tokenType = 'Bearer' }) {
		const token = await jwtSignAsync({
			user: {
				id,
				role,
				email,
			},
		}, this.config.jwt.secret);
		return `${tokenType} ${token}`;
	}
}

module.exports = { UserService };
