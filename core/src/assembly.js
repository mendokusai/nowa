const http = require('http');

const express = require('express');
const socketIO = require('socket.io');
const config = require('config');
const mongoose = require('mongoose');
const Promise = require('bluebird');

const { logger } = require('../lib/logger');

const { DbInitializerController } = require('./controllers/dbInitializerController');
const { DbInitializerService } = require('./interceptors/dbInitializerService');

const { CampaignController } = require('./controllers/campaignController');
const { JoinCampaignRequestController } = require('./controllers/joinCampaignRequestController');
const { UserController } = require('./controllers/userController');

const { CampaignService } = require('./interceptors/campaignService');
const { JoinCampaignService } = require('./interceptors/joinCampaignService');
const { UserService } = require('./interceptors/userService');

const { Router } = require('./router');

const { ERRORS } = require('./errors');

class Assembly {
	async init() {
		this.logger = logger;

		this.config = config;

		this.errors = ERRORS;

		this.connections = await this._initConnections();

		this.httpService = await this._initHttpService();

		this.httpServer = await this._initHttpServer(this);

		this.io = await this._initSocketIO(this);

		await this._initAppInstances();

		this.router = new Router(this);
	}

	async _initAppInstances() {
		if (process.env.NODE_ENV === 'develop') {
			this.dbInitializerService = new DbInitializerService(this);
			this.dbInitializerController = new DbInitializerController(this);
		}

		// order is important
		this.campaignService = new CampaignService(this);
		this.joinCampaignService = new JoinCampaignService(this);
		this.userService = new UserService(this);

		this.campaignController = new CampaignController(this);
		this.userController = new UserController(this);
		this.joinCampaignRequestController = new JoinCampaignRequestController(this);
	}

	async _initConnections() {
		try {
			mongoose.Promise = Promise;
			await mongoose.connect(this.config.mongo.url, this.config.mongo.options);
			this.logger.info('Connected to mongo!');
			return { mongo: mongoose.connection };
		} catch (error) {
			this.logger.error('Could not connect to mongo');
			throw error;
		}
	}

	async _initHttpService() {
		return express();
	}

	async _initHttpServer({ httpService }) {
		return http.createServer(httpService);
	}

	async _initSocketIO({ httpServer }) {
		return socketIO(httpServer);
	}
}

module.exports = {
	Assembly,
};
