const CODES = {
	TOKEN_IS_MISSING: 'TOKEN_IS_MISSING',
	INVALID_TOKEN: 'INVALID_TOKEN',
	UNAUTHORIZED: 'UNAUTHORIZED',
	ALREADY_JOINED_TO_CAMPAIGN: 'ALREADY_JOINED_TO_CAMPAIGN',
	BAD_REQUEST: 'BAD_REQUEST',
};

module.exports = {
	TOKEN_IS_MISSING: {
		httpStatusCode: 401,
		code: CODES.TOKEN_IS_MISSING,
		message: 'Authorization token is missing.',
	},
	INVALID_TOKEN: {
		httpStatusCode: 401,
		code: CODES.INVALID_TOKEN,
		message: 'Invalid token.',
	},
	UNAUTHORIZED: {
		httpStatusCode: 403,
		code: CODES.UNAUTHORIZED,
		message: 'You don\'t have the permission to access this content.',
	},
	EMAIL_OR_PASSWORD_IS_NOT_CORRECT: {
		httpStatusCode: 400,
		code: CODES.EMAIL_OR_PASSWORD_IS_NOT_CORRECT,
		message: 'Email or password is not correct.',
	},
	ALREADY_JOINED_TO_CAMPAIGN: {
		httpStatusCode: 400,
		code: CODES.ALREADY_JOINED_TO_CAMPAIGN,
		message: 'You are already joined to the campaign.',
	},
	BAD_REQUEST: {
		httpStatusCode: 400,
		code: CODES.BAD_REQUEST,
		message: 'Bad request!',
	},
};
