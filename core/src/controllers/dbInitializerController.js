class DbInitializerController {
	constructor({ dbInitializerService }) {
		this.dbInitializerService = dbInitializerService;
	}

	async init(req, res, next) {
		// TODO check admin token and password and other stuff
		try {
			await this.dbInitializerService.init();
			res.status(200).send();
		} catch (e) {
			next(e);
		}
	}
}

module.exports = { DbInitializerController };
