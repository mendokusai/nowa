const { existOrError } = require('../../lib/utils');

const { CampaignMapper } = require('../mappers/campaignMapper');
const { JoinCampaignRequestMapper } = require('../mappers/joinCampaignRequestMapper');

class CampaignController {
	constructor({ campaignService, joinCampaignService }) {
		existOrError(campaignService, 'campaignService');
		existOrError(joinCampaignService, 'joinCampaignService');

		this.campaignService = campaignService;
		this.joinCampaignService = joinCampaignService;
	}

	async createCampaign(req, res, next) {
		const { campaignData } = req.body;
		const userId = req.user.id;
		try {
			const { campaign } = await this.campaignService.createCampaign({ ...campaignData, userId });
			res.status(201).json({ campaign: CampaignMapper.fromModelToDto(campaign) });
		} catch (e) {
			next(e);
		}
	}

	// TODO add pagination, ...
	async getCampaignsList(req, res, next) {
		const userId = req.user.id;
		try {
			const { campaigns } = await this.campaignService.getCampaignsOfUser(userId);
			res.json({ campaigns: campaigns.map((c) => CampaignMapper.fromModelToDto(c)) });
		} catch (e) {
			next(e);
		}
	}

	async requestToJoin(req, res, next) {
		const userId = req.user.id;
		const { campaignCode } = req.params;
		try {
			const { joinCampaignRequest } = await this.joinCampaignService.requestToJoin(userId, campaignCode);
			res.status(201).json({ joinCampaignRequest: JoinCampaignRequestMapper.fromModelToDto(joinCampaignRequest) });
		} catch (e) {
			next(e);
		}
	}
}

module.exports = {
	CampaignController,
};
