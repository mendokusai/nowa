
const { existOrError } = require('../../lib/utils');

class UserController {
	constructor({ userService }) {
		existOrError(userService, 'userService');
		this.userService = userService;
	}

	async login(req, res, next) {
		try {
			const { email, password } = req.body;
			const { token } = await this.userService.login(email, password);
			res.json({ token });
		} catch (e) {
			next(e);
		}
	}

	async register(req, res, next) {
		const { userData } = req.body;
		try {
			// This must be a temporary token
			const { token } = await this.userService.register(userData)
			res.status(201).json({ token });
		} catch (e) {
			next(e);
		}
	}
}

module.exports = { UserController };
