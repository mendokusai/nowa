const { existOrError } = require('../../lib/utils');

const { JoinCampaignRequestMapper } = require('../mappers/joinCampaignRequestMapper');

class JoinCampaignRequestController {
	constructor({ joinCampaignService }) {
		existOrError(joinCampaignService, 'joinCampaignService');

		this.joinCampaignService = joinCampaignService;
	}

	async accept(req, res, next) {
		const userId = req.user.id;
		const { joinCampaignRequestId } = req.params;
		try {
			const { joinCampaignRequest } = await this.joinCampaignService.acceptRequest(userId, joinCampaignRequestId);
			res.json({ joinCampaignRequest: JoinCampaignRequestMapper.fromModelToDto(joinCampaignRequest) });
		} catch (e) {
			next(e);
		}
	}

	async reject(req, res, next) {
		const userId = req.user.id;
		const { joinCampaignRequestId } = req.params;
		try {
			const { joinCampaignRequest } = await this.joinCampaignService.rejectRequest(userId, joinCampaignRequestId);
			res.json({ joinCampaignRequest: JoinCampaignRequestMapper.fromModelToDto(joinCampaignRequest) });
		} catch (e) {
			next(e);
		}
	}

	async cancel(req, res, next) {
		const userId = req.user.id;
		const { joinCampaignRequestId } = req.params;
		try {
			const { joinCampaignRequest } = await this.joinCampaignService.cancelRequest(userId, joinCampaignRequestId);
			res.json({ joinCampaignRequest: JoinCampaignRequestMapper.fromModelToDto(joinCampaignRequest) });
		} catch (e) {
			next(e);
		}
	}

	// TODO pagination
	async getRequestsList(req, res, next) {
		const userId = req.user.id;
		const { role, status } = req.query;
		try {
			const { joinCampaignRequests } = await this.joinCampaignService.getRequests(userId, role, status);
			res.json({ joinCampaignRequests: joinCampaignRequests.map((it) => JoinCampaignRequestMapper.fromModelToDto(it)) });
		} catch (e) {
			next(e);
		}
	}
}

module.exports = { JoinCampaignRequestController };
