const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const Promise = require('bluebird');

const { existOrError } = require('../lib/utils');
const { errorHandler, GeneralError } = require('../lib/error-handler');

const jwtVerifyAsync = Promise.promisify(jwt.verify);

class Router {
	constructor({ logger, config, errors, httpService, dbInitializerController,
		campaignController, userController, joinCampaignRequestController }) {
		existOrError(logger, 'logger');
		existOrError(config, 'config');
		existOrError(errors, 'errors');
		existOrError(httpService, 'httpService');
		existOrError(campaignController, 'campaignController');
		existOrError(userController, 'userController');
		existOrError(joinCampaignRequestController, 'joinCampaignRequestController');

		existOrError(campaignController.createCampaign, 'createCampaign');
		existOrError(campaignController.getCampaignsList, 'getCampaignsList');
		existOrError(campaignController.requestToJoin, 'requestToJoin');

		existOrError(userController.login, 'login');
		existOrError(userController.register, 'register');

		existOrError(joinCampaignRequestController.accept, 'joinCampaignRequestController.accept');
		existOrError(joinCampaignRequestController.reject, 'joinCampaignRequestController.reject');
		existOrError(joinCampaignRequestController.cancel, 'joinCampaignRequestController.cancel');
		existOrError(joinCampaignRequestController.cancel, 'joinCampaignRequestController.cancel');
		existOrError(joinCampaignRequestController.getRequestsList, 'getRequestsList');


		this.logger = logger;
		this.config = config;
		this.errors = errors;
		this.httpService = httpService;
		this.dbInitializerController = dbInitializerController;

		this.campaignController = campaignController;
		this.userController = userController;
		this.joinCampaignRequestController = joinCampaignRequestController;

		this.handleError = errorHandler(this.logger);
	}

	registerRoutes() {
		this.httpService.use(bodyParser.json());
		this.httpService.use(
			bodyParser.urlencoded({ extended: true }),
		);

		/* eslint-disable consistent-return */
		// decode jwt
		this.httpService.use(async (req, res, next) => {
			const tokenStr = req.headers.authorization;
			if (tokenStr) {
				const tokenParts = tokenStr.split(' ', 2);
				const tokenType = tokenParts[0];
				const token = tokenParts[1];
				if (!token || tokenType !== 'Bearer') {
					next(new GeneralError(this.errors.INVALID_TOKEN));
				} else {
					try {
						const decoded = await jwtVerifyAsync(token, this.config.jwt.secret);
						req.user = decoded.user;
						next();
					} catch (e) {
						next(new GeneralError(this.errors.INVALID_TOKEN));
					}
				}
			} else {
				next();
			}
		});
		/* eslint-enable consistent-return */


		// here must be all the APIs that doesn't need any authentication.
		this.httpService.post('/users/register', (req, res, next) => this.userController.register(req, res, next));
		this.httpService.post('/users/login', (req, res, next) => this.userController.login(req, res, next));

		// TODO just admin should do that
		if (process.env.NODE_ENV === 'develop') {
			this.httpService.post('/db/init', (req, res, next) => this.dbInitializerController.init(req, res, next));
		}


		/* eslint-disable consistent-return */
		// handle token is missing
		this.httpService.use((req, res, next) => {
			if (req.user) {
				next();
			} else {
				next(new GeneralError(this.errors.TOKEN_IS_MISSING));
			}
		});
		/* eslint-enable consistent-return */

		// here must be all the APIs that need authentication.
		this.httpService.post('/campaigns', (req, res, next) => this.campaignController.createCampaign(req, res, next));
		this.httpService.get('/campaigns', (req, res, next) => this.campaignController.getCampaignsList(req, res, next));
		this.httpService.post('/campaigns/:campaignCode/join', (req, res, next) => this.campaignController.requestToJoin(req, res, next));

		this.httpService.put('/joinCampaignRequests/:joinCampaignRequestId/accept', (req, res, next) => this.joinCampaignRequestController.accept(req, res, next));
		this.httpService.put('/joinCampaignRequests/:joinCampaignRequestId/reject', (req, res, next) => this.joinCampaignRequestController.reject(req, res, next));
		this.httpService.put('/joinCampaignRequests/:joinCampaignRequestId/cancel', (req, res, next) => this.joinCampaignRequestController.cancel(req, res, next));
		this.httpService.get('/joinCampaignRequests', (req, res, next) => this.joinCampaignRequestController.getRequestsList(req, res, next));

		// here must be all the APIs that need authorization. make sure to use "authorization(roles)" method.

		this.httpService.use(errorHandler(this.logger));
	}

	/* eslint-disable consistent-return */
	authorization(roles) {
		return (req, res, next) => {
			if (!roles || roles.length < 1) {
				return next();
			}
			if (req.user.role && roles.includes(req.user.role)) {
				return next();
			}
			next(new GeneralError(this.errors.UNAUTHORIZED));
		};
	}
	/* eslint-enable consistent-return */
}

module.exports = {
	Router,
};
