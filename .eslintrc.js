module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'no-underscore-dangle': 'off',
    'class-methods-use-this': 'off',
    indent: ['error', 'tab'],
    'no-tabs': 'off',
    'max-len': ['error', { comments: 180, code: 160 }],
    'object-curly-newline': 'off',
  },
};
